# README #

The takehome.c program intakes a comma-separated value (CSV) file that contains rows of:


```
#!c

UNIX time, temperature, other stuff 1, other stuff 2, ...

```


It outputs a statistical summary based on temperature data which outlines the following: Mean, median, mode, maximum, minimum, and standard deviation.

### How to use the takehome program ###

* takehome.c can be compiled using gcc and makes use of the C standard libraries 


```
#!c

HOME$    gcc -o takehome takehome.c

HOME$    ./takehome

```


### Running the test files ###


* Input the name of the CSV file to use for analysis. The CSV file is included in the same folder as takehome.
* 11 test files are included for testing purposes