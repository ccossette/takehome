#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#define OK       0
#define NO_INPUT 1
#define TOO_LONG 2

// These are all part of C's standard library as defined in https://en.wikipedia.org/wiki/C_standard_library

typedef struct Sensor {
    // Unix time
    unsigned long int time;
    
    // Temperature in Kelvins
    float temp;
	   
} Sensor;

int tempCompare (const void * a, const void * b);
int isNumeric (const char * s);
static int getLine (char *prompt, char *buffer, size_t size);

void findMode(Sensor *sortedTemps, int size);

int main(int argc, char **argv) {
    int line;
    char buf[40];
    
    //************** CSV File Input *************************
    
    line = getLine ("Enter the name of the CSV file to analyze> ", buf, sizeof(buf));
    if (line == NO_INPUT) {
        printf ("\nNo input\n");
        return 1;
    }
    
    if (line == TOO_LONG) {
        printf ("Input too long [%s]\n", buf);
        return 1;
    }
    
    //************** CSV File Parsing ************************
    
    // Stream pointer
    FILE *tempFileStream;
    // Maximum length of a string
    char *buffer = malloc(512);
    
    // Open the csv file
    FILE *tempFile = fopen(buf, "r");
    
    if (tempFile == NULL) {
        printf("Error opening file\n");
        // Exit the program
        return 0;
    }
    
    // Create an array of sensor structs using dynamic memory allocation because
    // we don't know the exact number of lines in the file and can't set the array size in advance
    int array_size = 5;// TODO set to a small size for testing
    Sensor *sensors = (Sensor*)malloc(array_size*sizeof(Sensor));
    int i = 0;
    char * timeToken;
    char * tempToken;
    int timeCheck;
    int tempCheck;
    
    // Skip the first line of the file, which is the header (this is an assumption)
    fgets(buffer, 255, tempFile);
    
    // Read each line of the file and stop when we hit NULL (end of file)
    while (fgets(buffer, 255, tempFile) != NULL) {
        // Get the unix time token
        timeToken = strtok(buffer, ",");
        //Store in temp value
        timeCheck = isNumeric(timeToken);
        
        // Now get the temp token
        tempToken = strtok(NULL, ",");
        //Store in temp value
        tempCheck = isNumeric(tempToken);
        
        
        // Reallocate the size of the array by doubling
        if (i >= array_size-1) {
            array_size *= 2;
            // Debug statement for memory allocation
            // printf("\nReallocating the array to size %i ", array_size);
            sensors = realloc(sensors, array_size*sizeof(struct Sensor));
            
            if(sensors == NULL) {
                printf("Error with Reallocation of struct array");
                exit(1);
            }
        }
        
        //************** Bad Data Removal ************************
        
        //Check for NaN values
        if (!(timeCheck)||!(tempCheck)) {
            printf("\n *** ERROR: NaN detected on line number %i ***\n", i);
        } else {
            sensors[i].time = atoi(timeToken);
            sensors[i].temp = atof(tempToken);
            // Increment the position in the array
            i++;
        }
    }
    
    //************** Statistical Calculations ******************
    
    //Length of CSV file
    int length = i-1;
    
    if (length < 1) {
        printf("\n***ERROR - FILE CONTAINS NO USABLE DATA \n");
        return 0;
    }
    
    //Sort array
    qsort(sensors, i, sizeof(Sensor), &tempCompare);
    
    //Find Max
    float max = sensors[i-1].temp;
    
    //Find Min
    float min = sensors[0].temp;
    
    //Find Mean
    float sum = 0;
    float  mean = 0;
    for (int k=0; k<i; k++) {
        sum = sum + sensors[k].temp;
    }
    mean = sum/i;
    
    //Find Median
    float median = 0;
    //If odd, output middle value
    if (i%2!=0) {
        if (i==1) {
            median = sensors[0].temp;
        } else {
            median = sensors[i/2 +1].temp;
        }
    } else {
        //If even, output average of middle two values
        median = (sensors[i/2].temp + sensors[i/2 +1].temp)/2;
    }
    
    //Find Stdev
    float stdDev=0;
    
    for (int k=0; k<i; k++) {
        stdDev = stdDev + ((sensors[k].temp - mean)*(sensors[k].temp - mean));
    }
    stdDev = sqrtf(stdDev / i);
    
    //************** Results Printout ***********************
    
    //Print Results
    printf("\n====================================================\nOSENSA Junior Software Engineer Take-home Assignment \nWritten by: Carlo Cossette\n====================================================\nStatistical summary of temperature data:");
    printf("\nMean:\t%f", mean);
    printf("\nMedian:\t%f", median);
    printf("\nMode:\t");
    
    //Find Mode
    findMode(sensors, length);
    
    printf("\nMax:\t%f", max);
    printf("\nMin:\t%f", min);
    printf("\nStdev:\t%f", stdDev);
    printf("\n====================================================\n");
    
    free (sensors);
    return 0;
}

//************** Quicksort Comparison *********************
int tempCompare (const void * a, const void * b) {
    float fa = ((struct Sensor *)a)->temp;
    float fb = ((struct Sensor *)b)->temp;
    return (fa > fb) - (fa < fb);
}


//************** NaN Detection ****************************
int isNumeric (const char * s) {
    if (s == NULL || *s == '\0' || isspace(*s))
        return 0;
    char * p;
    strtod (s, &p);
    return *p == '\0';
}

//************** CSV File Name Buffer *********************
static int getLine (char *prompt, char *buf, size_t size) {
    int x, y;
    
    if (prompt != NULL) {
        printf ("%s", prompt);
        fflush (stdout);
    }
    if (fgets (buf, size, stdin) == NULL)
        return NO_INPUT;
    
    if (buf[strlen(buf)-1] != '\n') {
        y = 0;
        while (((x = getchar()) != '\n') && (x != EOF))
            y = 1;
        return (y == 1) ? TOO_LONG : OK;
    }
    
    buf[strlen(buf)-1] = '\0';
    return OK;
}

//************** Hash Table Definition *********************
// Contains a linked list of k, v nodes
typedef struct HashTable {
    int size;
    // a linked list of hashnodes
    struct HashNode **nodes;
    
} HashTable;

typedef struct HashNode {
    float temp;
    int occurrences;
    // points to the next item in the list
    struct HashNode *next;
} HashNode;

HashNode *findFloat(HashTable *ht, float temp) {
    // Create intermediary list for lookup
    HashNode *list = *ht->nodes;
    
    // Return a NULL if the element is not in the list
    // otherwise return the pointer to the element
    while(list != NULL) {
        if (list->temp == temp) {
            return list;
        }
        list = list->next;
    }
    
    return NULL;
}

//********* Finding Highest Occurence of Temp Data *********

int findMaxOccurrence(HashTable *ht) {
    // Create intermediary list for lookup
    HashNode *list = *ht->nodes;
    int maxOccurrence = 0;
    
    while(list != NULL) {
        if (list->occurrences > maxOccurrence) {
            maxOccurrence = list->occurrences;
        }
        list = list->next;
    }
    
    free(list);
    return maxOccurrence;
}

//******* Input Temp Data/Occurences Into Hash Table *********

void findMode(Sensor *sortedTemps, int num) {
    int size = num +1;
    int mode = 0;
    int nodeNumber = 0;
    // Construct a hashtable to store hashed float keys and occurrences
    HashTable *hashtable = malloc(sizeof(HashTable));
    // Allocate space for the linked list inside the hashtable
    // Set to the max size of the array, in case all of the values are unique
    hashtable->nodes = malloc(sizeof(HashNode*) * size);
    // Now initialize all the elements to NULL to start with
    for(int i=0;i<size; i++) {
        hashtable->nodes[i] = NULL;
    }
    // Set the size of the hashtable
    hashtable->size = size;
    
    // Scan the sorted array and count up the occurrences of each temp
    for (int i=0; i<size; i++) {
        // Increment if the value exists, otherwise create a new entry
        HashNode *position = findFloat(hashtable, sortedTemps[i].temp);
        
        if (position == NULL) {
            // Create a new node for this float hash
            HashNode *newNode = malloc(sizeof(HashNode*));
            newNode->temp = sortedTemps[i].temp;
            newNode->occurrences = 1;
            nodeNumber++;
            // Add the new node to the first of the list
            if (hashtable->nodes == NULL) {
                newNode->next = NULL;
                *hashtable->nodes = newNode;
            } else {
                HashNode *tmp = *hashtable->nodes;
                *hashtable->nodes = newNode;
                newNode->next = tmp;
            }
            
        } else {
            // Increment the value at this position
            position->occurrences += 1;
        }
    }
    //********* Isolate Temp Values That Qualify as Mode ***********
    //Build a Mode array
    int modes = findMaxOccurrence(hashtable);
    float allModes[size];
    int numberOfModes = 0;
    
    // Create intermediary list for lookup
    HashNode *node = *hashtable->nodes;
    
    while (node!=NULL) {
        if (node->occurrences == modes) {
            allModes[numberOfModes] = node->temp;
            numberOfModes++;
        }
        node = node->next;
        
    }
    
    //********* Define Modal Distribution and Printout **************
    
    //Check for Uniform Distribution
    if (numberOfModes == size) {
        numberOfModes = 0;
    }
    
    //Print out all modes
    switch (numberOfModes) {
        case 0: {
            printf("No mode detected, Uniform Distribution");
            break;
        }
        case 1: {
            printf("%f, ", allModes[0]);
            printf("Unimodal Distribution");
            break;
        }
        case 2: {
            printf("%f, ", allModes[0]);
            printf("%f, ", allModes[1]);
            printf("Bimodal Distribution");
            break;
        }
        default: {
            
            for (int i=0; i<numberOfModes; i++) {
                printf("%f, ", allModes[i]);
            }
            printf("Multimodal Distribution");
        }
            
    }
    
    // Deallocate memory from the hashtable and the nodes inside too
    free(hashtable);
    free(node);
    return;
}


